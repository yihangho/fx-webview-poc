package sample;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

public class Controller {
    @FXML
    private WebView webView;

    @FXML
    private TextField textField;

    public void initialize() {
        WebEngine engine = webView.getEngine();
        engine.load(Controller.class.getResource("index.html").toString());

        JSObject window = (JSObject) engine.executeScript("window");
        window.setMember("controller", this);
    }

    @FXML
    public void addMessage() {
        WebEngine engine = webView.getEngine();
        String script = "addMessage('" + textField.getText() + "')";
        engine.executeScript(script);
    }

    public void setMessage(String message) {
        System.out.println(message);
    }
}
